class ArticlesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index] #Helper de devise para pedir autenticacion antes de ejecutas acciones
  before_action :set_article, except: [:index, :new, :create] #Codigo mas limpio buscando el articulo con callback

  def index
  	@articles = Article.all
  end

  def show
    @article.update_visits_count
    @comment = Comment.new #Necesario para comentarios en articulo
  end

  #Crea un articulo en blanco para enviarlo al formulario
  def new
  	@article = Article.new
    @categories = Category.all
  end


  def create
  	@article = current_user.articles.new(article_params)
    @article.categories = params[:categories]
   	if @article.save
   		redirect_to @article
   	else
   		render :new
   	end
  end

  def destroy
    @article.destroy
    redirect_to articles_path
  end

  def edit
    
  end

  def update
    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :cover, :categories)
    end

    def set_article
      @article = Article.find(params[:id])
    end

end
