Rails.application.routes.draw do
  resources :categories
  resources :articles do #Los comentarios son un recurso de los articulos
  	resources :comments, only: [:create, :destroy, :update]#Ya no se podrá acceder a /comments directo, sino /articles/:id/comments
  end
  devise_for :users

  get 'welcome/index'
  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
